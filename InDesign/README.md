# Jonathan Conley's Portfolio InDesign Section


# InDesign
## Business Card Project:
![Business_Card](img/Conley_Company_Card.png)
#### Requirements:
* Import graphics from Logo Choice
* Manually creating pages within InDesign
* Creativity
* Concept
* Execution

## Business Letterhead Project:
![Business_Letterhead](img/Conley_Letterhead.png)
#### Requirements:
* 8.5" x 11" Portrait
* 72 ppi resolution
* Import graphics from Logo Choice
* Manually creating pages within InDesign
* Creativity
* Concept
* Execution