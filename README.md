# Jonathan Conley's Portfolio

This is a repository full of Projects that I have completed through semesters of being an Information Technology major at Florida State University. I am expecting to graduate Spring of 2017 with a specialization in Design & Development. 

## Links to each individual section
#### [Photoshop](Photoshop)
#### [Illustrator](Illustrator)
#### [InDesign](InDesign)


# Photoshop
## Who Am I [1/2]:
![About_You_1](Photoshop/img/Conley_Final1.png)
## Who Am I [2/2]:
![About_You_2](Photoshop/img/Conley_Final2.png)
#### Requirements:
* Headline or title
* Body of 200 words or more
* Call to action
* Convert all text to outlines
* Photos with 72 ppi resolution
* Correct bleeds

## Steph Curry Magazine Cover:
![Curry](Photoshop/img/Conley_Curry.png)
#### Requirements:
* 8.5" x 11" portrait
* 72 ppi resolution
* Multiple labeled layers showing at least 3 images
* Effective use of type
* Gradient 
* Layer styles (FX)
* Adjustment layer
* Layer mask

## Wish You Were Here Card:
![Wish_You_Were_Here_Card](Photoshop/img/Conley_Card.png)
Beginning Photoshop Experiment




# Illustrator
## Logos Project:
![Logos](Illustrator/img/Conley_Logos.png)
#### Requirements:
* Create logos using draw and shapes tool
* Creativity
* Concept
* Execution

## Name Project [1/2]:
![Name_1](Illustrator/img/Conley_Name1.png)
#### Requirements:
* Use a simple a sentence to describe yourself and things that you like
* Utilize shapes tool
* Creativity
* Concept
* Execution

## Name Project [2/2]:
![Name_2](Illustrator/img/Conley_Name2.png)
#### Requirements:
* Use a simple a sentence to describe yourself and things that you like
* Utilize shapes tool
* Creativity
* Concept
* Execution




# InDesign
## Business Card Project:
![Business_Card](InDesign/img/Conley_Company_Card.png)
#### Requirements:
* Import graphics from Logo Choice
* Manually creating pages within InDesign
* Creativity
* Concept
* Execution

## Business Letterhead Project:
![Business_Letterhead](InDesign/img/Conley_Letterhead.png)
#### Requirements:
* 8.5" x 11" Portrait
* 72 ppi resolution
* Import graphics from Logo Choice
* Manually creating pages within InDesign
* Creativity
* Concept
* Execution
