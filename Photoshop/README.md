# Jonathan Conley's Portfolio Photoshop Section


# Photoshop
## Who Am I [1/2]:
![About_You_1](img/Conley_Final1.png)
## Who Am I [2/2]:
![About_You_2](img/Conley_Final2.png)
#### Requirements:
* Headline or title
* Body of 200 words or more
* Call to action
* Convert all text to outlines
* Photos with 72 ppi resolution
* Correct bleeds

## Steph Curry Magazine Covery:
![Curry](img/Conley_Curry.png)
#### Requirements:
* 8.5" x 11" portrait
* 72 ppi resolution
* Multiple labeled layers showing at least 3 images
* Effective use of type
* Gradient 
* Layer styles (FX)
* Adjustment layer
* Layer mask

## Wish You Were Here Card:
![Wish_You_Were_Here_Card](img/Conley_Card.png)
Beginning Photoshop Experiment