# Jonathan Conley's Portfolio InDesign Section


# Illustrator
## Logos Project:
![Logos](img/Conley_Logos.png)
#### Requirements:
* Create logos using draw and shapes tool
* Creativity
* Concept
* Execution

## Name Project [1/2]:
![Name_1](img/Conley_Name1.png)
#### Requirements:
* Use a simple a sentence to describe yourself and things that you like
* Utilize shapes tool
* Creativity
* Concept
* Execution

## Name Project [2/2]:
![Name_2](img/Conley_Name2.png)
#### Requirements:
* Use a simple a sentence to describe yourself and things that you like
* Utilize shapes tool
* Creativity
* Concept
* Execution